using Game.Scripts.Audio;
using Game.Scripts.Core;
using Game.Scripts.LevelingAndStats;
using Game.Scripts.Movement;
using RPG.Combat;
using UnityEngine;

namespace Game.Scripts.Combat
{
    public class Fighter : MonoBehaviour, IAction
    {
        [SerializeField] private float attackRange = 2f;
        [SerializeField] float timeBetweenAttacks = 1f;
        [SerializeField] private float damage = 5f;
        [SerializeField] private Projectile projectile;
        [SerializeField] private Transform pointForSpawnProjectile;
        
        private Stats _target;
        private float _timeSinceLastAttack = Mathf.Infinity;
        private void Update()
        {
            _timeSinceLastAttack += Time.deltaTime;  
            
            if (_target == null ) return;
            if (_target.IsDead()) return;
            if (!GetIsInRange())
            {
                GetComponent<Mover>().MoveTo(_target.transform.position);
            }
            else
            {
                
                GetComponent<Mover>().Cancel();
                AttackBehaviour();
            }
        }

        private void AttackBehaviour()
        {
            transform.LookAt(_target.transform);
            if (_timeSinceLastAttack > timeBetweenAttacks)
            {
                TriggerAttack();
                _timeSinceLastAttack = 0;


            }
            
        }

        private void TriggerAttack()
        {
            GetComponent<Animator>().ResetTrigger("stopAttack");
            GetComponent<Animator>().SetTrigger("attack");
        }

        
        // Animation event
        private void Hit()
        {
            if (_target == null) return;
            if (projectile == null)
            {
                _target.TakeDamage(damage);
                if (CompareTag("Player"))
                {
                    AudioManager.Instance.PlaySfx("PlayerAttack");
                }
                else
                {
                    AudioManager.Instance.PlaySfx("SkeletonSwordAttack");
                }
            }
            else
            {
                LaunchProjectile();
            }
        }

        private bool GetIsInRange()
        {
            return Vector3.Distance(transform.position, _target.transform.position) < attackRange;
        }

        private void LaunchProjectile()
        {
            Projectile projectileInstance = Instantiate(projectile, pointForSpawnProjectile.position, pointForSpawnProjectile.rotation);
            projectileInstance.SetTarget(_target, damage);
        }

        public bool CanAttack(GameObject combatTarget)
        {
            if (combatTarget == null) return false;
            if (combatTarget == gameObject) return false;
            Stats targetToTest = combatTarget.GetComponent<Stats>();
            return targetToTest != null && !targetToTest.IsDead();
        }


        public void Attack(GameObject combatTarget)
        {
            GetComponent<ActionScheduler>().StartAction(this);
            _target = combatTarget.GetComponent<Stats>();
        }

        public void Cancel()
        {
            StopAttack();
            _target = null;
        }

        private void StopAttack()
        {
            GetComponent<Animator>().ResetTrigger("attack");
            GetComponent<Animator>().SetTrigger("stopAttack");
        }

        public void SetWeaponDamage(int newDamage)
        {
            damage = newDamage;
        }


    }
}

