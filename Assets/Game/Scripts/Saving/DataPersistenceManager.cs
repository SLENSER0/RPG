using System;
using System.Collections.Generic;
using System.Linq;
using Game.Scripts.Inventory;
using Game.Scripts.Questing;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.Scripts.Saving
{
    public class DataPersistenceManager : MonoBehaviour
    {
        [Header("File Storage Config")]
        [SerializeField] private string fileName;
        [SerializeField] private bool useEncryption;

        [SerializeField] private InventoryObject inventory;
        [SerializeField] private InventoryObject equipment;
        [SerializeField] private QuestDatabase questDatabase;

        private GameData _gameData;
        private List<IDataPersistence> _dataPersistenceObjects;
        private FileDataHandler _dataHandler;

        public static DataPersistenceManager instance { get; private set; }

        private void Awake() 
        {
            if (instance != null) 
            {
                Debug.LogError("Found more than one Data Persistence Manager in the scene.");
                Destroy(this.gameObject);
                return;
            }
            instance = this;
            DontDestroyOnLoad(this.gameObject);
            
            this._dataHandler = new FileDataHandler(Application.persistentDataPath, fileName, useEncryption);
        }

        private void OnEnable()
        {
            SceneManager.sceneLoaded += OnSceneLoaded;
            SceneManager.sceneUnloaded += OnSceneUnloaded;
        }

        private void OnDisable()
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
            SceneManager.sceneUnloaded -= OnSceneUnloaded;
        }

        private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            this._dataPersistenceObjects = FindAllDataPersistenceObjects();
            LoadGame();
        }
        
        private void OnSceneUnloaded(Scene scene)
        {
            SaveGame();
        }
        
        public void NewGame() 
        {
            this._gameData = new GameData();
            inventory.Clear();
            inventory.Save();
            equipment.Clear();
            equipment.Clear();
            
            foreach (var quest in questDatabase.Quests)
            {
                quest.IsAccepted = false;
                quest.IsCompleted = false;
                quest.IsQuestReadyForComplete = false;
                
                if (quest is KillEnemiesQuest killQuest)
                {
                    killQuest.CurrentKills = 0;
                }
            }
        }

        public void LoadGame()
        {
            this._gameData = _dataHandler.Load();
        
            if (this._gameData == null) 
            {
                Debug.Log("No data was found. Initializing data to defaults.");
                return;
            }

            foreach (IDataPersistence dataPersistenceObj in _dataPersistenceObjects) 
            {
                dataPersistenceObj.LoadData(_gameData);
            }
            LoadQuestData();
            
        }

        public void SaveGame()
        {
            if (this._gameData==null)
            {
                return;
            }
            
            foreach (IDataPersistence dataPersistenceObj in _dataPersistenceObjects) 
            {
                dataPersistenceObj.SaveData(_gameData);
            }
            
            SaveQuestData();

            _dataHandler.Save(_gameData);
        }

        private void OnApplicationQuit() 
        {
            SaveGame();
        }

        private List<IDataPersistence> FindAllDataPersistenceObjects() 
        {
            IEnumerable<IDataPersistence> dataPersistenceObjects = FindObjectsOfType<MonoBehaviour>()
                .OfType<IDataPersistence>();

            return new List<IDataPersistence>(dataPersistenceObjects);
        }

        public bool HasGameData()
        {
            return _gameData != null;
        }
        private void SaveQuestData()
        {
            if (this._gameData == null)
                return;

            this._gameData.QuestsData.Clear();

            foreach (Quest quest in questDatabase.Quests)
            {
                QuestData questData = new QuestData
                {
                    QuestId = quest.QuestId,
                    IsCompleted = quest.IsCompleted,
                    IsQuestReadyForComplete = quest.IsQuestReadyForComplete,
                    IsAccepted = quest.IsAccepted,
                };

                this._gameData.QuestsData.Add(questData);
            }
        }
        private void LoadQuestData()
        {
            if (this._gameData == null)
                return;

            foreach (QuestData questData in this._gameData.QuestsData)
            {
                Quest quest = questDatabase.Quests.Find(q => q.QuestId == questData.QuestId);
                if (quest != null)
                {
                    quest.IsCompleted = questData.IsCompleted;
                    quest.IsQuestReadyForComplete = questData.IsQuestReadyForComplete;
                    quest.IsAccepted = questData.IsAccepted;
                }
            }
        }
    }
}