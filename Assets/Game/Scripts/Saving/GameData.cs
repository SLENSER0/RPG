
using System.Collections.Generic;
using UnityEngine.Serialization;

namespace Game.Scripts.Saving
{
    [System.Serializable]
    public class GameData
    {
        [FormerlySerializedAs("currentLevel")] public int CurrentLevel;
        [FormerlySerializedAs("currrntXp")] public int CurrrntXp;
        [FormerlySerializedAs("baseStrength")] public int BaseStrength;
        [FormerlySerializedAs("baseStamina")] public int BaseStamina;
        [FormerlySerializedAs("baseIntelligence")] public int BaseIntelligence;
        [FormerlySerializedAs("currentStatPoints")] public int CurrentStatPoints;

        public float CurrentHP;
        public float CurrentMana;
        public float MaxHP;
        public float MaxMana;
        
        public List<QuestData> QuestsData;

        
        public GameData()
        {
            this.CurrentLevel = 1; 
            this.CurrrntXp = 10;
            this.BaseStrength = 0;
            this.BaseStamina = 0;
            this.BaseIntelligence = 0;
            this.CurrentStatPoints = 0;
            this.CurrentHP = 100;
            this.CurrentMana = 100;
            this.MaxHP = 100;
            this.MaxMana = 100;
            
            this.QuestsData = new List<QuestData>();
        }
    }
    [System.Serializable]
    public class QuestData
    {
        [FormerlySerializedAs("questId")] public int QuestId;
        [FormerlySerializedAs("isCompleted")] public bool IsCompleted;
        [FormerlySerializedAs("isQuestReadyForComplete")] public bool IsQuestReadyForComplete;
        [FormerlySerializedAs("isAccepted")] public bool IsAccepted;
    }
    
    
}

