﻿namespace Game.Scripts.Core
{
    public interface IAction
    {
        void Cancel();
    }
}