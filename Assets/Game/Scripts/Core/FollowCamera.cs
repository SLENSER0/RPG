using UnityEngine;

namespace Game.Scripts.Core
{
    public class FollowCamera : MonoBehaviour
    {
        [SerializeField] private Transform target;
        [SerializeField] private float rotationSpeed = 5f;
        [SerializeField] private float zoomSpeed = 5f;
        [SerializeField] private float minZoomDistance = 1f;
        [SerializeField] private float maxZoomDistance = 10f;

        private float _currentZoomDistance = 5f;
        private Vector3 _offset;
        private bool _rotateCamera = false;
        private Vector3 _lastMousePosition;

        private void Start()
        {
            _offset = transform.position - target.position;
        }

        private void LateUpdate()
        {
            float zoomInput = Input.GetAxis("Mouse ScrollWheel");
            float zoomAmount = -zoomInput * zoomSpeed;
            float newZoomDistance = _currentZoomDistance + zoomAmount;

            if (newZoomDistance >= minZoomDistance && newZoomDistance <= maxZoomDistance)
            {
                _currentZoomDistance = newZoomDistance;
            }

            if (Input.GetMouseButton(2))
            {
                Vector3 mouseDelta = Input.mousePosition - _lastMousePosition;
                float rotationX = mouseDelta.x * rotationSpeed * Time.deltaTime;

                transform.RotateAround(target.position, Vector3.up, rotationX);
            }

            Vector3 zoomVector = -transform.forward * _currentZoomDistance;
            transform.position = target.position + zoomVector;

            _lastMousePosition = Input.mousePosition;
        }
        
    }
}

