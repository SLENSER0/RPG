using UnityEngine;
using UnityEngine.Serialization;

namespace Game.Scripts.Core
{
    public class TransparencyController : MonoBehaviour
    {
        [FormerlySerializedAs("transparencyValue")] public float TransparencyValue = 0.5f;
        [FormerlySerializedAs("player")] public GameObject Player;

        private Renderer _objectRenderer;

        private void Start()
        {
            _objectRenderer = GetComponent<MeshRenderer>();
        }

        private void Update()
        {

            if (IsPlayerBehindObject())
            {
                print("test");
                SetObjectTransparency(TransparencyValue); 
            }
            else
            {
                SetObjectTransparency(1f);
            }
        }

        private bool IsPlayerBehindObject()
        {
            Vector3 directionToPlayer = Player.transform.position - transform.position;

            if (Physics.Raycast(transform.position, directionToPlayer, out RaycastHit hitInfo))
            {
                if (hitInfo.collider.gameObject == Player)
                {
                    return true;
               
                }
            }

            return false;
        }

        private void SetObjectTransparency(float transparency)
        {
            Color objectColor = _objectRenderer.material.color;
            objectColor.a = transparency;

            _objectRenderer.material.color = objectColor;
        
        }
    }
}
