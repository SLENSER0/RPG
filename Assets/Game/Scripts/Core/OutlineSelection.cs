using UnityEngine;

namespace Game.Scripts.Core
{
    [RequireComponent(typeof(Outline))]
    public class OutlineSelection : MonoBehaviour
    {
        private Outline _outline;

        private void Start()
        {
            _outline = GetComponent<Outline>();
            _outline.OutlineColor = Color.red;
            _outline.OutlineWidth = 0f;
        }

        private void OnMouseEnter()
        {
            _outline.OutlineWidth = 2f;
        }

        private void OnMouseExit()
        {
            _outline.OutlineWidth = 0f;
        }
    }
}