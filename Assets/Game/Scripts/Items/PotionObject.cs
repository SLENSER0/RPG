using UnityEngine;

namespace Game.Scripts.Items
{
    [CreateAssetMenu(fileName = "New Potion Object", menuName = "Inventory System/Items/Potion")]
    public class PotionObject : ItemObject
    {
        [SerializeField] private float restoreHealthValue;
        private void Awake()
        {
            type = ItemType.Potion;
        }
    }
}
