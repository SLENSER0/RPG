using Game.Scripts.LevelingAndStats;
using UnityEngine;

namespace Game.Scripts.Items
{
    public enum ItemType
    {
        Potion,
        Helmet,
        Weapon,
        Shield,
        Chest,
        Legs,
        Boots,
        Default
    }

    public enum Attributes
    {
        Strength,
        Stamina,
        Intelligence
    }

    public abstract class ItemObject : ScriptableObject
    {
        [SerializeField] internal Sprite uiDisplay;
        [SerializeField] internal ItemType type;
        [TextArea(15, 20)]
        [SerializeField] protected string description;
        [SerializeField] internal Item data = new Item();

        public bool Stackable;


        public Item CreateItem()
        {
            Item newItem = new Item(this);
            return newItem;
        }
    
    }

    [System.Serializable]
    public class Item
    {
        [SerializeField] internal string name;
        [SerializeField] internal int id;
        [SerializeField] internal ItemBuff[] buffs;
    
        public Item()
        {
            name = "";
            id = -1;
        }
        public Item(ItemObject item)
        {
            name = item.name;
            id = item.data.id;
            buffs = new ItemBuff[item.data.buffs.Length];
            for (int i = 0; i < buffs.Length; i++)
            {
            
                buffs[i] = new ItemBuff(item.data.buffs[i].minValue, item.data.buffs[i].maxValue)
                {
                    attribute = item.data.buffs[i].attribute
                };
            }
        }
    }
    [System.Serializable]
    public class ItemBuff : IModifier
    {
        [SerializeField] internal Attributes attribute;
        [SerializeField] internal int value;
        [SerializeField] internal int maxValue;
        [SerializeField] internal int minValue;

        public ItemBuff(int _minValue, int _maxValue)
        {
            maxValue = _maxValue;
            minValue = _minValue;
            GenerateValue();
        }

        public void GenerateValue()
        {
            value = UnityEngine.Random.Range(minValue, maxValue+1);
        }

        public void AddValue(ref int baseValue)
        {
            baseValue += value;
        }
    }
}