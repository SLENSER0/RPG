using UnityEngine;

namespace Game.Scripts.Items
{
    [CreateAssetMenu(fileName = "New Equipment Object", menuName = "Inventory System/Items/Equipment")]
    public class EquipmentObject : ItemObject
    {
    
    }
}
