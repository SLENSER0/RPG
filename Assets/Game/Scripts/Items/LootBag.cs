using UnityEngine;
using UnityEngine.Serialization;

namespace Game.Scripts.Items
{
    public class LootBag : MonoBehaviour
    {
        [System.Serializable]
        public class DropEntry
        {
            public ItemObject Item;
            public GameObject ItemGameObject;
            public float DropChance;
        }

        [FormerlySerializedAs("dropEntries")] public DropEntry[] DropEntries;

        public void DropItems()
        {
            float destroyinstantiatedItem = 45f;
            foreach (var entry in DropEntries)
            {
                if (Random.value <= entry.DropChance)
                {
                    entry.ItemGameObject.GetComponent<GroundItem>().item = entry.Item;
                    var instantiatedItem = Instantiate(entry.ItemGameObject, transform.position, Quaternion.identity);
                
                    Destroy(instantiatedItem, destroyinstantiatedItem);
                }
            }
        }
    }
}
