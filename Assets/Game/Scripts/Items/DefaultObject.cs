using UnityEngine;

namespace Game.Scripts.Items
{
    [CreateAssetMenu(fileName = "New Default Object", menuName = "Inventory System/Items/Default")]
    public class DefaultObject : ItemObject
    {
        private void Awake()
        {
            type = ItemType.Default;
        }
    }
}
