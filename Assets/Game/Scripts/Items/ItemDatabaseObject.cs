using System.Collections.Generic;
using UnityEngine;

namespace Game.Scripts.Items
{
    [CreateAssetMenu(fileName = "New Item Database", menuName = "Inventory System/Items/Database ")]
    public class ItemDatabaseObject : ScriptableObject, ISerializationCallbackReceiver
    {
        [SerializeField] internal ItemObject[] itemObjects;
    
        internal Dictionary<int, ItemObject> GetItem = new Dictionary<int, ItemObject>();
    
        public void OnBeforeSerialize()
        {
            GetItem = new Dictionary<int, ItemObject>();
        }

        public void OnAfterDeserialize()
        {
            for (int i = 0; i < itemObjects.Length; i++)
            {
                itemObjects[i].data.id = i;
                GetItem.Add(i,itemObjects[i]);
            }
        }
    }
}
