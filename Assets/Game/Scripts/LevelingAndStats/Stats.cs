using Game.Scripts.Combat;
using Game.Scripts.Control;
using Game.Scripts.Core;
using Game.Scripts.Items;
using Game.Scripts.Questing;
using Game.Scripts.UI;
using UnityEngine;
using UnityEngine.Serialization;

namespace Game.Scripts.LevelingAndStats
{
    public class Stats : MonoBehaviour
    {
        public int Id;
        [FormerlySerializedAs("questDatabase")] public QuestDatabase QuestDatabase;
        public QuestUI QuestUI;
        [FormerlySerializedAs("spawnPoint")] public Transform SpawnPoint;
        
        [SerializeField] private int xpReward;
        
        [Header("Health & Mana")] 
        [SerializeField] internal float currentHealthPoints = 100f;

        [SerializeField] internal float maxHealth = 100f;
        [SerializeField] internal float currentManaPoints = 0;
        [SerializeField] internal float maxMana;
        [SerializeField] private float healthRegenRate = 2f;
        [SerializeField] private float manaRegenRate = 1f;

       

        [Header("Conversions")] 
        [SerializeField] private int staminaToHealthConversion = 10;
        [SerializeField] private int strengthToDamageConversion = 2;
        [SerializeField] private int intelligenceToManaConversion = 10;

        private float _baseHealth = 100;
        private float _baseMana = 100;
        private float _baseDamage = 5;
        
        private LootBag _lootBag;
        private bool _isDead = false;
        private float _respawnTimer = 0f;
        private float _respawnDelay = 5f;
        
        private GameObject _player;
        

        private void Start()
        {
            _player = GameObject.FindGameObjectWithTag("Player");
            _lootBag = GetComponent<LootBag>();
        }

        private void Update()
        {
            if (!gameObject.CompareTag("Player")) return;
            if (currentHealthPoints <= 0f)
            {
                Die();
                currentHealthPoints = 0f;
            }
            if (_isDead && _respawnTimer > 0f)
            {
                _respawnTimer -= Time.deltaTime;

                if (_respawnTimer <= 0f)
                {
                    Respawn();
                }
            }
            if (currentHealthPoints <= maxHealth)
            {
                currentHealthPoints += healthRegenRate * Time.deltaTime;
                currentHealthPoints = Mathf.Clamp(currentHealthPoints, 0f, maxHealth);
            }

            if (currentManaPoints <= maxMana)
            {
                currentManaPoints += manaRegenRate * Time.deltaTime;
                currentManaPoints = Mathf.Clamp(currentManaPoints, 0f, maxMana);
            }
            
            maxHealth = _baseHealth + _player.GetComponent<Player>().Attributes[1].Value.ModifiedValue * staminaToHealthConversion;
            maxMana = _baseMana + _player.GetComponent<Player>().Attributes[2].Value.ModifiedValue * intelligenceToManaConversion;
            _player.GetComponent<Fighter>().SetWeaponDamage((int)_baseDamage+_player.GetComponent<Player>().Attributes[0].Value.ModifiedValue*strengthToDamageConversion);
            
        }

        public bool IsDead()
        {
            return _isDead;
        }

        public void TakeDamage(float damage)
        {
            currentHealthPoints = Mathf.Max(currentHealthPoints - damage, 0);
            if (currentHealthPoints == 0) Die();

           

        }

        public void KillQuestProgression()
        {
            if (QuestDatabase == null) return;
            foreach (Quest quest in QuestDatabase.Quests)
            {
                if (quest.IsAccepted)
                {
                    if (quest is KillEnemiesQuest killQuest)
                    {
                        killQuest.EnemyKilled(Id);
                       _player.GetComponent<Stats>().QuestUI.CreateQuestLog();
                    }
                }

            }
           
        }
        
        public float GetHealth()
        {
            return currentHealthPoints;
        }

        public float GetMaxHealth()
        {
            return maxHealth;
        }

        public float GetMana()
        {
            return currentManaPoints;
        }
        
        public float GetMaxMana()
        {
            return maxMana;
        }

        private void Die()
        {
            if (_isDead) return;
            _isDead = true;
            GetComponent<Animator>().SetTrigger("die");
            GetComponent<ActionScheduler>().CancelCurrentAction();
            if (!gameObject.CompareTag("Player"))
            {
                _player.GetComponent<XPTracker>().AddXP(xpReward);
                if (_lootBag != null)
                {
                    _lootBag.DropItems();
                }
                KillQuestProgression();
                
            }
            else
            {
                _respawnTimer = _respawnDelay;
                
            }

        }
        
        private void Respawn()
        {
            currentHealthPoints = maxHealth;
            currentManaPoints = maxMana;
            _isDead = false;

            transform.position = SpawnPoint.position;
            transform.rotation = SpawnPoint.rotation;

            GetComponent<Animator>().SetTrigger("Respawn");
            
            _respawnTimer = 0f;
        }

    }
}