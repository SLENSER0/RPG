namespace Game.Scripts.LevelingAndStats
{
    public interface IModifier
    {
        void AddValue(ref int baseValue);
    }
}
