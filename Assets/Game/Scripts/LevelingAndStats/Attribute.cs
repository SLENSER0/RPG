using Game.Scripts.Control;
using Game.Scripts.Items;
using UnityEngine.Serialization;

namespace Game.Scripts.LevelingAndStats
{
    [System.Serializable]
    public class Attribute
    {
        [System.NonSerialized]
        public Player Parent;
        [FormerlySerializedAs("type")] public Attributes Type;
        [FormerlySerializedAs("value")] public ModifiableInt Value;
    
        public void SetParent(Player _parent)
        {
            Parent = _parent;
            Value = new ModifiableInt(AttributeModified);
        }
        public void AttributeModified()
        {
            Parent.AttributeModified(this);
        }
        public void IncreaseValue()
        {
            Value.BaseValue += 1;
        }
    }
}
