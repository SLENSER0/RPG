using UnityEngine;

namespace Game.Scripts.LevelingAndStats
{
    public abstract class BaseXPTranslation : ScriptableObject
    {
        public int CurrentXP  = 0;
        public int CurrentLevel = 1;
        public int XPRequiredForNextLevel => GetXPRequiredForNextLevel();

        public int XPForNextLevel => GetXPForNextLevel();

        public int XPForCurrentLevel => GetXPForCurrentLevel();
        
        public bool AtLevelCap { get; protected set; } = false;

        public abstract bool AddXP(int amout);
        public abstract void SetLevel(int level);
        protected abstract int GetXPRequiredForNextLevel();

        protected abstract int GetXPForNextLevel();
        protected abstract int GetXPForCurrentLevel();

        public abstract void LoadLevelAndXp(int xp);

    }
}

