using Game.Scripts.Combat;
using Game.Scripts.Inventory;
using Game.Scripts.Items;
using Game.Scripts.LevelingAndStats;
using Game.Scripts.Movement;
using Game.Scripts.Questing;
using Game.Scripts.Saving;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using Attribute = Game.Scripts.LevelingAndStats.Attribute;

namespace Game.Scripts.Control
{
    public class Player : MonoBehaviour, IDataPersistence
    {
        enum CursorType
        {
            None,
            Movement,
            Combat
            
        }
        [System.Serializable]
        struct CursorMapping
        {
            public CursorType Type;
            [FormerlySerializedAs("texture")] public Texture2D Texture;
            [FormerlySerializedAs("hotspot")] public Vector2 Hotspot;
        }

        [FormerlySerializedAs("cursorMapping")] [SerializeField] private CursorMapping[] cursorMappings = null;
        

        [SerializeField] private int statPoints = 0;
        [SerializeField] private int statsPerLevel = 5;
        
        
        [SerializeField] internal InventoryObject inventory;
        [SerializeField] internal InventoryObject equipment;

        [SerializeField] private TextMeshProUGUI currentStrengthText;
        [SerializeField] private TextMeshProUGUI currentStaminaText;
        [SerializeField] private TextMeshProUGUI currentIntelligenceText;
        [SerializeField] private TextMeshProUGUI currentStatPointsText;

        [SerializeField] private float healthPotionRestore = 75f;
        [SerializeField] private float manaPotionRestore = 75f;
        
        private Stats _stats;

        private int _baseStrength = 0;
        private int _baseStamina = 0;
        private int _baseIntelligence = 0;
        private int _currentXp = 10;
        private float _maxHP = 100;
        private float _maxMana = 100;
        private float _CurrentHP = 100;
        private float _currentMana = 100;
        
        [FormerlySerializedAs("attributes")] public Attribute[] Attributes;
        //public MouseItem MouseItem = new MouseItem();
        
        public void LoadData(GameData data)
        {
  
            inventory.Load();
            equipment.Load();

            statPoints = data.CurrentStatPoints;

            _currentXp = data.CurrrntXp;
            
            _baseStrength = data.BaseStrength;
            _baseStamina = data.BaseStamina;
            _baseIntelligence = data.BaseIntelligence;

            _maxHP = data.MaxHP;
            _maxMana = data.MaxMana;
            _CurrentHP = data.CurrentHP;
            _currentMana = data.CurrentMana;
            Debug.Log(data.CurrentHP);

            
        }

        public void SaveData(GameData data)
        {

            inventory.Save();
            equipment.Save();
            
            data.CurrentStatPoints = statPoints;

            data.BaseStrength = _baseStrength;
            data.BaseStamina = _baseStamina;
            data.BaseIntelligence = _baseIntelligence;
            data.CurrrntXp = _currentXp;
            
            
            
            data.CurrentHP = _stats.currentHealthPoints;
            data.CurrentMana = _stats.currentManaPoints;
            data.MaxMana = _stats.maxMana;
            data.MaxHP = _stats.maxHealth;

        }
        
        private void Start()
        {
            _stats = GetComponent<Stats>();


            
            GetComponent<XPTracker>().LoadLevelAndXp(_currentXp);
            
            for (int i = 0; i < Attributes.Length; i++)
            {
                Attributes[i].SetParent(this);
            }

            for (int i = 0; i < equipment.GetSlots.Length; i++)
            {
                equipment.GetSlots[i].OnBeforeUpdate += OnBeforeSlotUpdate;
                equipment.GetSlots[i].OnAfterUpdate += OnAfterSlotUpdate;
            }
            
            Attributes[(int)Items.Attributes.Stamina].Value.BaseValue = _baseStamina;
            Attributes[(int)Items.Attributes.Strength].Value.BaseValue = _baseStrength;
            Attributes[(int)Items.Attributes.Intelligence].Value.BaseValue = _baseIntelligence;

            _stats.maxHealth = _maxHP;
            _stats.currentHealthPoints = _CurrentHP;
            _stats.maxMana = _maxMana;
            _stats.currentManaPoints = _currentMana;
            

            UpdateText();

        }

        public void OnBeforeSlotUpdate(InventorySlot _slot)
        {
            if (_slot.ItemObject == null)
            {
                return;
            }
            if (_slot.Parent.inventoryObject.type == InterfaceType.Equipment)
            {
                if (_slot.Parent.inventoryObject.type == InterfaceType.Equipment)
                {
                    for (int i = 0; i < _slot.item.buffs.Length; i++)
                    {
                        for (int j = 0; j < Attributes.Length; j++)
                        {
                            if (Attributes[j].Type == _slot.item.buffs[i].attribute)
                            {
                                Attributes[j].Value.RemoveModifier(_slot.item.buffs[i]);
                            }
                        }
                    }
                }
            }

            UpdateText();
        }
        public void OnAfterSlotUpdate(InventorySlot _slot)
        {
            if (_slot.ItemObject == null)
            {
                return;
            }
            if (_slot.Parent.inventoryObject.type == InterfaceType.Equipment)
            {
                for (int i = 0; i < _slot.item.buffs.Length; i++)
                {
                    for (int j = 0; j < Attributes.Length; j++)
                    {
                        if (Attributes[j].Type == _slot.item.buffs[i].attribute)
                        {
                            Attributes[j].Value.AddModifier(_slot.item.buffs[i]);
                        }
                    }
                }
            }

            UpdateText();
        }
        //Update stats text in equipment menu
        public void UpdateText()
        {
            foreach (Attribute attribute in Attributes)
            {
                switch (attribute.Type)
                {
                    
                    case Items.Attributes.Strength:
                        currentStrengthText.text = attribute.Value.ModifiedValue.ToString();
                        break;
                    case Items.Attributes.Stamina:
                        currentStaminaText.text = attribute.Value.ModifiedValue.ToString();
                        break;
                    case Items.Attributes.Intelligence:
                        currentIntelligenceText.text = attribute.Value.ModifiedValue.ToString();
                        break;
                    default:
                        break;
                }
            }
            currentStatPointsText.text = statPoints.ToString();

        }

        private void Update()
        {
            if (_stats.IsDead()) return;

            _currentXp = GetComponent<XPTracker>().GetCurrentXP();
            if (Input.GetKeyDown(KeyCode.Q))
            {
                if (inventory.DecreaseHealthPotion())
                {
                    _stats.currentHealthPoints = Mathf.Min(_stats.currentHealthPoints+healthPotionRestore,_stats.maxHealth);
                }
                
            }
            if (Input.GetKeyDown(KeyCode.B))
            {
                if (inventory.DecreaseManaPotion())
                {
                    _stats.currentManaPoints = Mathf.Min(_stats.currentManaPoints+manaPotionRestore,_stats.maxMana);
                }
                
            }
            
            if(EventSystem.current.IsPointerOverGameObject()) { return; }
            
            if (InteractWithCombat()) return;
            if (InteractWithMovement()) return;
            if (InteractWithQuest()) return;
            
            SetCursor(CursorType.None);
            
            

        }
        
        private bool InteractWithQuest()
        {
            RaycastHit hit;
            bool hasHit = Physics.Raycast(GetMouseRay(), out hit);
            if (hasHit)
            {
                QuestGiver questGiver = hit.transform.GetComponent<QuestGiver>();
                if (questGiver == null) return false;

                float distance = Vector3.Distance(transform.position, questGiver.transform.position);
                if (distance <= 5f)
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        questGiver.ShowQuestWindow();
                    }
                }
                else
                {
                    GetComponent<Mover>().StartMoveAction(questGiver.transform.position);
                }

                return true;
            }

            return false;
        }



        private bool InteractWithCombat()
        {
            
            RaycastHit[] hits = Physics.RaycastAll(GetMouseRay());
            foreach (RaycastHit hit in hits)
            {
                CombatTarget target = hit.transform.GetComponent<CombatTarget>();
                if (target == null) continue;
                

                if (!GetComponent<Fighter>().CanAttack(target.gameObject))
                {
                    continue;
                }

                if (Input.GetMouseButtonDown(0))
                {
                    GetComponent<Fighter>().Attack(target.gameObject);
                }

                SetCursor(CursorType.Combat); 
                return true;
            }
            return false;
        }

        private void SetCursor(CursorType type)
        {
            CursorMapping mapping = GetCursorMapping(type);
            Cursor.SetCursor(mapping.Texture, mapping.Hotspot, CursorMode.Auto);
        }

        private CursorMapping GetCursorMapping(CursorType type)
        {
            foreach (CursorMapping mapping in cursorMappings)
            {
                if (mapping.Type == type)
                {
                    return mapping;
                }
            }

            return cursorMappings[0];
        }

        private bool InteractWithMovement()
        {
            
            RaycastHit hit;
            bool hasHit = Physics.Raycast(GetMouseRay(), out hit);
            if (hasHit)
            {
                if (Input.GetMouseButton(0))
                {
                    GetComponent<Mover>().StartMoveAction(hit.point);
                }
                SetCursor(CursorType.Movement); 
                return true;
            }

            return false;
        }
        

        private static Ray GetMouseRay()
        {
            return Camera.main.ScreenPointToRay(Input.mousePosition);
        }
        
        private void OnDestroy()
        {

            inventory.Clear();
            equipment.Clear();

        }
        
        private void OnTriggerEnter(Collider other)
        {
            var item = other.GetComponent<GroundItem>();
            if (item)
            {
                Item _item = new Item(item.item);
                if (inventory.AddItem(_item,1))
                {
                    Destroy(other.gameObject);
                }

            }
        }
        
        public void AttributeModified(Attribute attribute)
        {
            Debug.Log(string.Concat(attribute.Type, " was updated! Value is now ", attribute.Value.ModifiedValue));
            
        }
        
        public void OnUpdateLevel(int previousLevel, int currentLevel)
        {
            statPoints += statsPerLevel;
            currentStatPointsText.text = statPoints.ToString();
            UpdateText();
        }
        
        public void IncreaseStrength()
        {
            if (statPoints > 0)
            {
                statPoints -= 1;
                Attributes[(int)Items.Attributes.Strength].IncreaseValue();
                _baseStrength = Attributes[(int)Items.Attributes.Strength].Value.BaseValue;
                UpdateText();
            }

        }

        public void IncreaseStamina()
        {
            if (statPoints > 0)
            {
                statPoints -= 1;
                Attributes[(int)Items.Attributes.Stamina].IncreaseValue();
                _baseStamina = Attributes[(int)Items.Attributes.Stamina].Value.BaseValue;
                UpdateText();
            }

        }

        public void IncreaseIntelligence()
        {
            if (statPoints > 0)
            {
                statPoints -= 1;
                Attributes[(int)Items.Attributes.Intelligence].IncreaseValue();
                _baseIntelligence = Attributes[(int)Items.Attributes.Intelligence].Value.BaseValue;
                UpdateText();
            }
        }

        /*public void SaveData(GameData data)
        {
            data.inventoryData.container = inventory.container;
        }

        public void LoadData(GameData data)
        {
            inventory.container = data.inventoryData.container;

        }*/
        
    }
    
}
