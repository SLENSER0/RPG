using System;
using UnityEngine;

namespace Game.Scripts.Audio
{
    public class AudioManager : MonoBehaviour
    {
        public static AudioManager Instance;
    
        public Sound[] MusicSounds, SfxSounds;
        public AudioSource MusicSource, SxfSource;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private void Start()
        {
            PlayMusic("Lament");
        }

        public void PlayMusic(string name)
        {
            Sound music = Array.Find(MusicSounds, x => x.Name == name);
            if (music != null)
            {
                MusicSource.clip = music.Clip;
                MusicSource.Play();
            }
        }
    
        public void PlaySfx(string name)
        {
            Sound sound = Array.Find(SfxSounds, x => x.Name == name);
            if (sound != null)
            {
                SxfSource.clip = sound.Clip;
                SxfSource.Play();
            }
        }
    }
}
