using UnityEngine;

namespace Game.Scripts.Audio
{
    [System.Serializable]
    public class Sound
    {
        public string Name;
        public AudioClip Clip;
    }
}
