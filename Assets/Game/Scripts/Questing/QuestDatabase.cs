using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace Game.Scripts.Questing
{
    [CreateAssetMenu(fileName = "QuestDatabase", menuName = "Quest/Quest Database")]
    public class QuestDatabase : ScriptableObject
    {
        [FormerlySerializedAs("quests")] public List<Quest> Quests = new List<Quest>();
    }
}
