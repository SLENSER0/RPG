﻿using Game.Scripts.Control;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Game.Scripts.Questing
{
    public class QuestWindow : MonoBehaviour
    {
        [FormerlySerializedAs("questWindowUI")] public GameObject QuestWindowUI;
        [FormerlySerializedAs("questTitleText")] public TextMeshProUGUI QuestTitleText;
        [FormerlySerializedAs("questDescriptionText")] public TextMeshProUGUI QuestDescriptionText;
        [FormerlySerializedAs("questDescriptionForAcceptedQuest")] public TextMeshProUGUI QuestDescriptionForAcceptedQuest;
        [FormerlySerializedAs("acceptButton")] public Button AcceptButton;
        [FormerlySerializedAs("declineButton")] public Button DeclineButton;

        private Player _player;
        private Quest _currentQuest;

        private void Start()
        {
            _player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        }

        public void ShowQuestWindow(Quest quest)
        {
            
            _currentQuest = quest;
            
            if (!_currentQuest.IsAccepted) {
            QuestTitleText.text = quest.QuestTitle;
            QuestDescriptionText.text = quest.QuestDescription;

            AcceptButton.onClick.RemoveAllListeners();
            AcceptButton.onClick.AddListener(() => AcceptQuest());

            DeclineButton.onClick.RemoveAllListeners();
            DeclineButton.onClick.AddListener(() => DeclineQuest());

            QuestWindowUI.SetActive(true);
            }
            else
            {

                QuestDescriptionForAcceptedQuest.text = quest.QuestDescriptionForAcceptedQuest;
                if (_currentQuest.IsQuestReadyForComplete)
                {
                    AcceptButton.GetComponentInChildren<TextMeshProUGUI>().text = "Complete";
                    AcceptButton.onClick.RemoveAllListeners();
                    AcceptButton.onClick.AddListener(() => CompleteQuest());
                }
                else
                {
                    AcceptButton.GetComponentInChildren<TextMeshProUGUI>().text = "Accept";
                    AcceptButton.onClick.RemoveAllListeners();
                    AcceptButton.onClick.AddListener(() => AcceptQuest());
                }
                DeclineButton.onClick.RemoveAllListeners();
                DeclineButton.onClick.AddListener(() => DeclineQuest());

                QuestWindowUI.SetActive(true);
            }
        }

        public void HideQuestWindow()
        {
            QuestWindowUI.SetActive(false);
        }

        public void AcceptQuest()
        {
            if (!_currentQuest.IsAccepted)
            {
                _currentQuest.IsAccepted = true;
            }
            HideQuestWindow();
            print(_currentQuest);
        }

        public void DeclineQuest()
        {
            _currentQuest.IsAccepted = false;
            HideQuestWindow();
        }

        public void CompleteQuest()
        {
            if (_currentQuest.IsQuestReadyForComplete)
            {
                print("Great!");
                _currentQuest.IsCompleted = true;
                HideQuestWindow();
            }
        }
    }
}