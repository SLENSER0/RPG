using UnityEngine;
using UnityEngine.Serialization;

namespace Game.Scripts.Questing
{
    [CreateAssetMenu(fileName = "New Kill Quest", menuName = "Quest/Quests/KillEnemiesQuest")]
    public class KillEnemiesQuest : Quest
    {
        public int EnemyId;
        [FormerlySerializedAs("requiredKills")] public int RequiredKills;
        [FormerlySerializedAs("currentKills")] public int CurrentKills = 0 ;



        public override bool IsReached()
        {
            return CurrentKills >= RequiredKills;
        
        }

        public override void Complete()
        {
        
        }

        public void EnemyKilled(int enemyId)
        {
            if (enemyId == EnemyId)
            {
                CurrentKills += 1;

                if (IsReached())
                {
                    IsQuestReadyForComplete = true;
                }
            }
        }
    }
}