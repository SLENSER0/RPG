using Game.Scripts.Items;
using UnityEngine;
using UnityEngine.Serialization;

namespace Game.Scripts.Questing
{
    [CreateAssetMenu(fileName = "New Gather Quest", menuName = "Quest/Quests/GatherItemsQuest")]
    public class GatherItemsQuest : Quest
    {
        public ItemObject ItemPrefab;
        [FormerlySerializedAs("itemsToGather")] public int ItemsToGather;

        public override bool IsReached()
        {
            throw new System.NotImplementedException();
        }

        public override void Complete()
        {
            throw new System.NotImplementedException();
        }
    }
}

