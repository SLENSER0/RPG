using UnityEngine;
using UnityEngine.Serialization;

namespace Game.Scripts.Questing
{
    [System.Serializable]
    public abstract class Quest : ScriptableObject
    {
        [FormerlySerializedAs("questId")] public int QuestId;
        [FormerlySerializedAs("questTitle")] public string QuestTitle;
        [FormerlySerializedAs("questDescription")] public string QuestDescription;
        [FormerlySerializedAs("questDescriptionForAcceptedQuest")] public string QuestDescriptionForAcceptedQuest;
        [FormerlySerializedAs("isCompleted")] public bool IsCompleted;
        [FormerlySerializedAs("isQuestReadyForComplete")] public bool IsQuestReadyForComplete;
        [FormerlySerializedAs("isAccepted")] public bool IsAccepted;
        [FormerlySerializedAs("questType")] public QuestType QuestType;
        [FormerlySerializedAs("questGiver")] public QuestGiver QuestGiver;
    
        public abstract bool IsReached();
        public abstract void Complete();
    }


    public enum QuestType
    {
        Kill,
        Gather
    }
}