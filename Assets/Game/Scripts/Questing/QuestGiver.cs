using System;
using Game.Scripts.Movement;
using UnityEngine;
using UnityEngine.Serialization;

namespace Game.Scripts.Questing
{
    [RequireComponent(typeof(BoxCollider))]
    public class QuestGiver : MonoBehaviour
    {
        public GameObject ExclamationPoint ;
        
        [FormerlySerializedAs("questDatabase")] public QuestDatabase QuestDatabase;
        [FormerlySerializedAs("questWindow")] public QuestWindow QuestWindow;
        [FormerlySerializedAs("questId")] public int QuestId;
        internal GameObject ExclamationPointSpawn;
        private GameObject _player;
        public float OffsetY;

        private void Start()
        {
            _player = GameObject.FindGameObjectWithTag("Player");
            
            Vector3 objectSize = GetComponent<MeshRenderer>().bounds.size;
            Vector3 spawnPosition = transform.position + Vector3.up * ((objectSize.y / 2f)+OffsetY);

            if (!QuestDatabase.Quests[QuestId].IsAccepted)
            {
                ExclamationPointSpawn =
                    Instantiate(ExclamationPoint, spawnPosition, ExclamationPoint.transform.rotation);
            }
        }

        private void Update()
        {
            if (QuestDatabase.Quests[QuestId].IsAccepted)
            {
                Destroy(ExclamationPointSpawn);
            }
        }

        private void OnMouseDown()
        {
            if (Input.GetMouseButtonDown(0))
            {
                print("1");
                float distance = Vector3.Distance(transform.position, _player.transform.position);
                if (distance <= 5f)
                {
                    ShowQuestWindow();
                }
                else
                {
                    _player.GetComponent<Mover>().MoveTo(transform.position);
                }
            }
        }

        public Quest GetQuestById(int questId)
        {
            foreach (Quest quest in QuestDatabase.Quests)
            {
                if (quest.QuestId == questId)
                    return quest;
            }
            return null;
        }
        

        public void ShowQuestWindow()
        {
            Quest quest = GetQuestById(QuestId);
            if (quest != null)
            {
                QuestWindow.ShowQuestWindow(quest);
            }
            
        }
        
    }
}