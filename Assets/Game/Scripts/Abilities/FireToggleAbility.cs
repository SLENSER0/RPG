using Game.Scripts.Control;
using Game.Scripts.LevelingAndStats;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Game.Scripts.Abilities
{
    public class FireToggleAbility : MonoBehaviour
    {
        [SerializeField] private float abilityManaCostEverySec = 10f;
        [SerializeField] private int abilityPower = 15;
        [SerializeField] private ParticleSystem abilityParticle;
        [FormerlySerializedAs("cooldownBackgroung")] [SerializeField] private Image cooldownBackground;

        private bool _isAbilityActive = false;
        private Stats _playerStats;
        private Player _player;
        private float _abilityTimer = 0f;


        private void Start()
        {
            _playerStats = GameObject.FindGameObjectWithTag("Player").GetComponent<Stats>();
            _player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                OnButtonClick();
            }
            if (_isAbilityActive)
            {
                
                _abilityTimer += Time.deltaTime;
                if (_abilityTimer >= 1f)
                {
                    _playerStats.currentManaPoints = Mathf.Max(0, _playerStats.currentManaPoints-abilityManaCostEverySec);
                    if (_playerStats.currentManaPoints <= 0 || _playerStats.IsDead())
                    {
                        DeactivateAbility();
                    }

                    _abilityTimer = 0f;
                }
            }
        }
        public void ActivateAbility()
        {
            if (_playerStats.currentManaPoints < abilityManaCostEverySec) return;
            var tempColor = cooldownBackground.color;
            tempColor.a = 0.8f;
            cooldownBackground.color = tempColor;
            _isAbilityActive = true;
            _abilityTimer = 1f;
            _player.GetComponent<Player>().Attributes[0].Value.ModifiedValue += abilityPower;

            if (abilityParticle != null)
            {
                abilityParticle.Play();
            }
        }
    
        private void DeactivateAbility()
        {
            _isAbilityActive = false;
            var tempColor = cooldownBackground.color;
            tempColor.a = 0.0f;
            cooldownBackground.color = tempColor;
            _abilityTimer = 0f;
            _player.GetComponent<Player>().Attributes[0].Value.ModifiedValue -= abilityPower;

            if (abilityParticle != null)
            {
                abilityParticle.Stop();
            }
        }

        public void OnButtonClick()
        {
            if (_isAbilityActive)
            {
                DeactivateAbility();
            }
            else
            {
                ActivateAbility();
            
            }
            _player.UpdateText();
        }
        
        

    }
}
