using Game.Scripts.LevelingAndStats;
using UnityEngine;

public class HealAbility : MonoBehaviour
{
    [SerializeField] private float healAmount = 50f;
    [SerializeField] private float cooldown = 5f;
    [SerializeField] private float manaRequirement = 35f;
    
    private bool _isOnCooldown = false;
    private Stats _playerStats;

    private void Start()
    {
        _playerStats = GetComponent<Stats>();
    }

    public bool TryHeal()
    {
        if (_isOnCooldown || _playerStats.IsDead() || _playerStats.currentManaPoints < manaRequirement)
            return false;

        _playerStats.currentManaPoints -= manaRequirement;
        _playerStats.currentHealthPoints += healAmount;
        _playerStats.currentHealthPoints = Mathf.Clamp(_playerStats.currentHealthPoints, 0f, _playerStats.maxHealth);
        StartCoroutine(StartCooldown());
        return true;
    }

    private System.Collections.IEnumerator StartCooldown()
    {
        _isOnCooldown = true;
        yield return new WaitForSeconds(cooldown);
        _isOnCooldown = false;
    }
}
