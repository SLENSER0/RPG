using Game.Scripts.LevelingAndStats;
using TMPro;
using UnityEngine;

namespace Game.Scripts.UI
{
    public class PlayerUILevel : MonoBehaviour
    {
        private GameObject _player;
        private XPTracker _xpTracker;
        private TextMeshProUGUI _textMesh;
        void Start()
        {
            _player = GameObject.FindGameObjectWithTag("Player");
            _xpTracker = _player.GetComponent<XPTracker>();
            _textMesh = GetComponentInChildren<TextMeshProUGUI>();

        }

        void Update()
        {
            _textMesh.text = _xpTracker.GetLevel().ToString();

        }
    }
}
