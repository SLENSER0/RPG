using System;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Scripts.UI
{
    public class HealButton : MonoBehaviour
    {
        [SerializeField] private HealAbility healAbility;

        private void Awake()
        {
            if (healAbility == null)
                healAbility = GameObject.FindGameObjectWithTag("Player").GetComponent<HealAbility>();

            Button button = GetComponent<Button>();
            if (button != null)
                button.onClick.AddListener(OnButtonClick);
        }

        private void OnButtonClick()
        {
            if (healAbility.TryHeal())
            {
                GetComponent<CooldownButton>().StartCooldown();
            }
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                OnButtonClick();
                
            }
        }
    }
}
