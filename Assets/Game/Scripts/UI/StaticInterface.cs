using System.Collections.Generic;
using Game.Scripts.Inventory;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Game.Scripts.UI
{
    public class StaticInterface : UserInterface
    {
        public GameObject[] Slots;

        public override void CreateSlots()
        {
            _slotsOnInterface = new Dictionary<GameObject, InventorySlot>();
            for (int i = 0; i < inventoryObject.GetSlots.Length; i++)
            {
                var obj = Slots[i];
            
                AddEvent(obj, EventTriggerType.PointerEnter, delegate { OnEnter(obj); });
                AddEvent(obj, EventTriggerType.PointerExit, delegate { OnExit(obj); });
                AddEvent(obj, EventTriggerType.BeginDrag, delegate { OnDragStart(obj); });
                AddEvent(obj, EventTriggerType.EndDrag, delegate { OnDragEnd(obj); });
                AddEvent(obj, EventTriggerType.Drag, delegate { OnDrag(obj); });
                inventoryObject.GetSlots[i].SlotDisplay = obj;
            
                _slotsOnInterface.Add(obj, inventoryObject.GetSlots[i]);
            }
        }
    }
}
