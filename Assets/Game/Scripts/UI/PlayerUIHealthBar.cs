using System;
using Game.Scripts.LevelingAndStats;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Scripts.UI
{
    public class PlayerUIHealthBar : MonoBehaviour
    {
        private GameObject _player;
        private Slider _slider;
        private TextMeshProUGUI _textMesh;

        void Start()
        {
            _player = GameObject.FindGameObjectWithTag("Player");
            _slider = GetComponent<Slider>();
            _textMesh = GetComponentInChildren<TextMeshProUGUI>();
        }


        void Update()
        {
            var maxHealth =  _player.GetComponent<Stats>().GetMaxHealth();
            var currentHealth = _player.GetComponent<Stats>().GetHealth();
            if ( !_player.GetComponent<Stats>().IsDead())
            {
                _slider.maxValue = maxHealth;
                _slider.value = currentHealth;
                _textMesh.text = String.Concat(Mathf.Round(currentHealth), "/", maxHealth);
            }
            else
            {
                _slider.value = 0f;
                _textMesh.text = String.Concat(0f, "/", maxHealth);
            }
;
        }
    }
}
