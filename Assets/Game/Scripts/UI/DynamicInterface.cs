using System.Collections.Generic;
using Game.Scripts.Inventory;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Game.Scripts.UI
{
    public class DynamicInterface : UserInterface
    {   
        [SerializeField] private GameObject inventoryPrefab;
        [SerializeField] private int xSpaceBetweenItems;
        [SerializeField] private int ySpaceBetweenItems;
        [SerializeField] private int numberOfColumn;
        [SerializeField] private int xStart;
        [SerializeField] private int yStart;
    
        public override void CreateSlots()
        { 
            _slotsOnInterface = new Dictionary<GameObject, InventorySlot>();
            for (int i = 0; i < inventoryObject.GetSlots.Length; i++)
            {
                var obj = Instantiate(inventoryPrefab, Vector3.zero, Quaternion.identity, transform);
                obj.GetComponent<RectTransform>().localPosition = GetPosition(i);

                AddEvent(obj, EventTriggerType.PointerEnter, delegate { OnEnter(obj); });
                AddEvent(obj, EventTriggerType.PointerExit, delegate { OnExit(obj); });
                AddEvent(obj, EventTriggerType.BeginDrag, delegate { OnDragStart(obj); });
                AddEvent(obj, EventTriggerType.EndDrag, delegate { OnDragEnd(obj); });
                AddEvent(obj, EventTriggerType.Drag, delegate { OnDrag(obj); });
            
                inventoryObject.GetSlots[i].SlotDisplay = obj;

                _slotsOnInterface.Add(obj, inventoryObject.GetSlots[i]);
            }
        }
    
        private Vector3 GetPosition(int i)
        {
            return new Vector3(xStart + (xSpaceBetweenItems * (i % numberOfColumn)), yStart + (-ySpaceBetweenItems * (i / numberOfColumn)),
                0f);
        }
    }
}
