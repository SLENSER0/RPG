using System;
using Game.Scripts.LevelingAndStats;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Scripts.UI
{
    public class PlayerUIManaBar : MonoBehaviour
    {
        private GameObject _player;
        private Slider _slider;
        private TextMeshProUGUI _textMesh;
        
        void Start()
        {
            _player = GameObject.FindGameObjectWithTag("Player");
            _slider = GetComponent<Slider>();
            _textMesh = GetComponentInChildren<TextMeshProUGUI>();
        }

        void Update()
        {
            if (_player.GetComponent<Stats>().IsDead()) return;
            var maxMana =  _player.GetComponent<Stats>().GetMaxMana();
            var currentMana = _player.GetComponent<Stats>().GetMana();
            _slider.maxValue = maxMana;
            _slider.value = currentMana;
            _textMesh.text = String.Concat(Mathf.Round(currentMana),"/",maxMana);
        
        }
    }
}
