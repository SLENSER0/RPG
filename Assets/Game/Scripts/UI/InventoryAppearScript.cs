using UnityEngine;
using UnityEngine.Serialization;

namespace Game.Scripts.UI
{
    public class InventoryAppearScript : MonoBehaviour
    {
        [FormerlySerializedAs("_inventoryPanel")] [SerializeField] private GameObject inventoryPanel;
        private CanvasGroup _canvasGroup;

        private void Start()
        {
            _canvasGroup = inventoryPanel.GetComponent<CanvasGroup>();
        }


        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.I))
            {
                ToggleInventory();
            }
        }

        private void ToggleInventory()
        {
            _canvasGroup.alpha = _canvasGroup.alpha == 0 ? 1 : 0;
            _canvasGroup.interactable = !_canvasGroup.interactable;
            _canvasGroup.blocksRaycasts = !_canvasGroup.blocksRaycasts;
        }
    

    }
}
