using Game.Scripts.LevelingAndStats;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Scripts.UI
{
    public class PlayerUIXPBar : MonoBehaviour
    {
        private GameObject _player;
        private XPTracker _xpTracker;
        private Slider _slider;
        void Start()
        {
            _player = GameObject.FindGameObjectWithTag("Player");
            _xpTracker = _player.GetComponent<XPTracker>();
            _slider = GetComponent<Slider>();

        }

        void Update()
        {
            _slider.maxValue = _xpTracker.GetXPForNextLevel();
            _slider.minValue = _xpTracker.GetXPForCurrentLevel();
            _slider.value = _xpTracker.GetCurrentXP();
        }
    }
}
