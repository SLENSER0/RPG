using UnityEngine;
using UnityEngine.UI;

namespace Game.Scripts.UI
{
    public class CooldownButton : MonoBehaviour
    {
        [SerializeField] private float cooldownDuration = 5f;
        private bool _isOnCooldown = false;
        private Image _cooldownImage;
        private Button _button;

        private float cooldownTimer = 0f;

        private void Start()
        {
            _cooldownImage = transform.GetChild(2).GetComponent<Image>();
            _button = GetComponent<Button>();
        }

        private void Update()
        {
            if (_isOnCooldown)
            {
                cooldownTimer += Time.deltaTime;
                float cooldownFill = 1f - (cooldownTimer / cooldownDuration);
                _cooldownImage.fillAmount = cooldownFill;

                if (cooldownTimer >= cooldownDuration)
                {
                    _isOnCooldown = false;
                    cooldownTimer = 0f;
                    _cooldownImage.fillAmount = 0f;
                    _button.interactable = true;
                }
            }
        }

        public void StartCooldown()
        {
            if (!_isOnCooldown)
            {
                _isOnCooldown = true;
                _button.interactable = false;
            }
        }
    }
}