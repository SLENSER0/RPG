using System.Collections.Generic;
using Game.Scripts.Inventory;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Game.Scripts.UI
{
    public abstract class UserInterface : MonoBehaviour
    {

        [SerializeField] internal InventoryObject inventoryObject;

        protected Dictionary<GameObject, InventorySlot> _slotsOnInterface = new Dictionary<GameObject, InventorySlot>();

        private void Start()
        {
        
            for (int i = 0; i < inventoryObject.GetSlots.Length; i++)
            {
                inventoryObject.GetSlots[i].Parent = this;
                inventoryObject.GetSlots[i].OnAfterUpdate += OnSlotUpdate;

            }
            CreateSlots();
            
            for (int i = 0; i < inventoryObject.GetSlots.Length; i++)
            {
                inventoryObject.GetSlots[i].UpdateSlot(inventoryObject.container.slots[i].item, inventoryObject.container.slots[i].amount);
            }
            
            AddEvent(gameObject, EventTriggerType.PointerEnter, delegate { OnEnterInterface(gameObject); });
            AddEvent(gameObject, EventTriggerType.PointerExit, delegate { OnExitInterface(gameObject); });
        }

        private void OnSlotUpdate(InventorySlot _slot)
        {
            if (_slot.item.id >= 0)
            {
                _slot.SlotDisplay.transform.GetChild(0).GetComponentInChildren<Image>().sprite =
                    _slot.ItemObject.uiDisplay;
                _slot.SlotDisplay.transform.GetChild(0).GetComponentInChildren<Image>().color = new Color(1, 1, 1, 1);
                _slot.SlotDisplay.GetComponentInChildren<TextMeshProUGUI>().text =
                    _slot.amount == 1 ? "" : _slot.amount.ToString("n0");
            }
            else
            {
                _slot.SlotDisplay.transform.GetChild(0).GetComponentInChildren<Image>().sprite = null;
                _slot.SlotDisplay.transform.GetChild(0).GetComponentInChildren<Image>().color = new Color(1, 1, 1, 0);
                _slot.SlotDisplay.GetComponentInChildren<TextMeshProUGUI>().text = "";
            }
        }

        /*private void Update()
    {
        _slotsOnInterface.UpdateSlotDisplay();
    }*/
    

        public abstract void CreateSlots();


        protected void AddEvent(GameObject obj, EventTriggerType type, UnityAction<BaseEventData> action)
        {
            EventTrigger trigger = obj.GetComponent<EventTrigger>();
            var eventTrigger = new EventTrigger.Entry();
            eventTrigger.eventID = type;
            eventTrigger.callback.AddListener(action);
            trigger.triggers.Add(eventTrigger);
        }

        public void OnEnter(GameObject obj)
        {
            MouseData.SlotHoveredOver = obj;
        }
        public void OnExit(GameObject obj)
        {
            MouseData.SlotHoveredOver = null;
        }
        public void OnExitInterface(GameObject obj)
        {
            MouseData.InterfaceMouseIsOver = null;
        }
        public void OnEnterInterface(GameObject obj)
        {
            MouseData.InterfaceMouseIsOver = obj.GetComponent<UserInterface>();
        }
        public void OnDragStart(GameObject obj)
        {
            MouseData.TempItemBeingDragged = CreateTempItem(obj);
        }

        public GameObject CreateTempItem(GameObject obj)
        {
            GameObject tempItem = null;
            if (_slotsOnInterface[obj].item.id >= 0)
            {
                tempItem = new GameObject();
                var rt = tempItem.AddComponent<RectTransform>();
                rt.sizeDelta = new Vector2(50, 50);
                tempItem.transform.SetParent(transform.parent);
                var img = tempItem.AddComponent<Image>();
                img.sprite = _slotsOnInterface[obj].ItemObject.uiDisplay;
                img.raycastTarget = false;
            }

            return tempItem;

        }
    
        public void OnDragEnd(GameObject obj)
        {
            Destroy(MouseData.TempItemBeingDragged);

            if (MouseData.InterfaceMouseIsOver == null)
            {
                _slotsOnInterface[obj].RemoveItem();
                return;
            }

            if (MouseData.SlotHoveredOver)
            {
                InventorySlot mouseHoverSlotData = MouseData.InterfaceMouseIsOver._slotsOnInterface[MouseData.SlotHoveredOver];
                inventoryObject.SwapItem(_slotsOnInterface[obj], mouseHoverSlotData);
            }
        }
        public void OnDrag(GameObject obj)
        {
            if (MouseData.TempItemBeingDragged != null)
            {
                MouseData.TempItemBeingDragged.GetComponent<RectTransform>().position = Input.mousePosition;
            }
        }


    
    }

    public static class MouseData
    {
        public static UserInterface InterfaceMouseIsOver;
        public static GameObject TempItemBeingDragged;
        public static GameObject HoverObject;
        public static GameObject SlotHoveredOver;
    }

    public static class ExtensionMethods
    {
        public static void UpdateSlotDisplay(this Dictionary<GameObject, InventorySlot> _slotsOnInterface)
        {
            foreach (KeyValuePair<GameObject, InventorySlot> _slot in _slotsOnInterface) 
            {
                if (_slot.Value.item.id >= 0)
                {
                    _slot.Key.transform.GetChild(0).GetComponentInChildren<Image>().sprite =
                        _slot.Value.ItemObject.uiDisplay;
                    _slot.Key.transform.GetChild(0).GetComponentInChildren<Image>().color = new Color(1, 1, 1, 1);
                    _slot.Key.GetComponentInChildren<TextMeshProUGUI>().text =
                        _slot.Value.amount == 1 ? "" : _slot.Value.amount.ToString("n0");
                }
                else
                {
                    _slot.Key.transform.GetChild(0).GetComponentInChildren<Image>().sprite = null;
                    _slot.Key.transform.GetChild(0).GetComponentInChildren<Image>().color = new Color(1, 1, 1, 0);
                    _slot.Key.GetComponentInChildren<TextMeshProUGUI>().text = "";
                }
            }
        }
    }
}