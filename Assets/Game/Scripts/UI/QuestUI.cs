using System;
using Game.Scripts.Questing;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Game.Scripts.UI
{
    public class QuestUI : MonoBehaviour
    {
        [FormerlySerializedAs("questDatabase")] public QuestDatabase QuestDatabase;
        public GameObject GridElement;
        public GameObject QuestLogCanvas;

        private CanvasGroup _canvasGroup;
    
        private void Start()
        {
            _canvasGroup = QuestLogCanvas.GetComponent<CanvasGroup>();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.L))
            {
                ShowHideQuestLog();
            }
        }

        private void ShowHideQuestLog()
        {
            _canvasGroup.alpha = _canvasGroup.alpha == 0 ? 1 : 0;
            _canvasGroup.interactable = !_canvasGroup.interactable;
            _canvasGroup.blocksRaycasts = !_canvasGroup.blocksRaycasts;
        
            CreateQuestLog();
        }

        public void CreateQuestLog()
        {
            foreach (Transform child in transform) {
                GameObject.Destroy(child.gameObject);
            }
        
            foreach (Quest quest in QuestDatabase.Quests)
            {
                if (quest.IsAccepted && !quest.IsCompleted)
                {
                    GameObject questPanel = Instantiate(GridElement, gameObject.transform);
                    questPanel.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = quest.QuestTitle;
                    questPanel.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = quest.QuestDescription;
                    questPanel.transform.GetChild(2).GetChild(0).GetComponent<Image>().enabled = quest.IsQuestReadyForComplete;
                    if (quest is KillEnemiesQuest killQuest)
                    {
                        questPanel.transform.GetChild(3).GetComponent<TextMeshProUGUI>().text = String.Concat(killQuest.CurrentKills,"/",killQuest.RequiredKills," were killed");
                    }
                }
            }
        }
    }
}
