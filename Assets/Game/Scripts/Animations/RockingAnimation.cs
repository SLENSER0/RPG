using UnityEngine;
using UnityEngine.Serialization;

namespace Game.Scripts.Animations
{
    public class RockingAnimation : MonoBehaviour
    {
        [FormerlySerializedAs("amplitude")] public float Amplitude = 0.1f;
        [FormerlySerializedAs("speed")] public float Speed = 1f;

        private Vector3 _startPosition;

        private void Start()
        {
            _startPosition = transform.position;
        }

        private void Update()
        {
            float newY = _startPosition.y + Mathf.Sin(Time.time * Speed) * Amplitude;
            transform.position = new Vector3(transform.position.x, newY, transform.position.z);
        }
    }
}
