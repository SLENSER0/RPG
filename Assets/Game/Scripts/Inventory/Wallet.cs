using TMPro;
using UnityEngine;

namespace Game.Scripts.Inventory
{
    public class Wallet : MonoBehaviour
    {
        public int Coins { get; private set; }
        [SerializeField] private TextMeshProUGUI currentCoinsText;

        public void AddCoins(int amount)
        {
            Coins += amount;
            currentCoinsText.text = Coins.ToString();
        }
    }
}
