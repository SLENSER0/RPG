using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Game.Scripts.Items;
using Game.Scripts.UI;
using UnityEngine;

namespace Game.Scripts.Inventory
{
    public enum InterfaceType
    {
        Inventory,
        Equipment
    }
 

    [CreateAssetMenu(fileName = "New Inventory", menuName = "Inventory System/Inventory")]
    public class InventoryObject : ScriptableObject
    {
        [SerializeField] private string savePath;
        [SerializeField] internal ItemDatabaseObject databaseObject;
        [SerializeField] internal Inventory container;
        [SerializeField] internal InterfaceType type; 
        public InventorySlot[] GetSlots
        {
            get { return container.slots; }
        }


        public bool AddItem(Item _item, int _amount)
        {
            if (EmptySlotCount <= 0)
            {
                return false;
            }

            InventorySlot slot = FindItemOnInventory(_item);
            if (!databaseObject.itemObjects[_item.id].Stackable || slot == null)
            {
                SetEmptySlot(_item, _amount);
                return true;
            }
            slot.AddAmount(_amount);
            return true;
        }

        public int EmptySlotCount
        {
            get
            {
                int counter = 0;
                for (int i = 0; i < GetSlots.Length; i++)
                {
                    if (GetSlots[i].item.id < 0)
                    {
                        counter++;
                    }
                }

                return counter;
            }
        }
        
        public int HealthPotionCount()
        {
            int healthPotionCount = 0;

            for (int i = 0; i < GetSlots.Length; i++)
            {
                InventorySlot slot = GetSlots[i];
                if (slot.item.name == "HealthPotion")
                {
                    healthPotionCount += slot.amount;
                }
            }

            return healthPotionCount;
        }
        
        public bool DecreaseHealthPotion()
        {
            if (HealthPotionCount() <= 0)
            {
                return false;
                
            }
            for (int i = 0; i < GetSlots.Length; i++)
            {
                InventorySlot slot = GetSlots[i];
                
                if (slot.item.name == "HealthPotion")
                {
                   slot.amount--;
                   if (slot.amount == 0)
                   {
                       slot.RemoveItem();
                   }
                }
            }
            
            
            
            for (int i = 0; i < GetSlots.Length; i++)
            {
                GetSlots[i].UpdateSlot(container.slots[i].item, container.slots[i].amount);
            }

            return true;
        }
        
        public int ManaPotionCount()
        {
            int manaPotionCount = 0;

            for (int i = 0; i < GetSlots.Length; i++)
            {
                InventorySlot slot = GetSlots[i];
                if (slot.item.name == "ManaPotion")
                {
                    manaPotionCount += slot.amount;
                }
            }

            return manaPotionCount;
        }
        
        public bool DecreaseManaPotion()
        {
            if (ManaPotionCount() <= 0)
            {
                return false;
                
            }
            for (int i = 0; i < GetSlots.Length; i++)
            {
                InventorySlot slot = GetSlots[i];
                
                if (slot.item.name == "ManaPotion")
                {
                    slot.amount--;
                    if (slot.amount == 0)
                    {
                        slot.RemoveItem();
                    }
                }
            }
            
            
            
            for (int i = 0; i < GetSlots.Length; i++)
            {
                GetSlots[i].UpdateSlot(container.slots[i].item, container.slots[i].amount);
            }

            return true;
        }


        public InventorySlot FindItemOnInventory(Item _item)
        {
            for (int i = 0; i < GetSlots.Length; i++)
            {
                if (GetSlots[i].item.id == _item.id)
                {
                    return GetSlots[i];
                }
            }
            return null;
        }
        public InventorySlot SetEmptySlot(Item _item, int _amount)
        {
            for (int i = 0; i < GetSlots.Length; i++)
            {
                if (GetSlots[i].item.id <= -1)
                {
                    GetSlots[i].UpdateSlot(_item, _amount);
                    return GetSlots[i];
                }
            }
            //set up functionality for full inventory
            return null;
        }

        /*private void OnEnable()
    {
        #if UNITY_EDITOR
        
        databaseObject = (ItemDatabaseObject)AssetDatabase.LoadAssetAtPath("Assets/Resources/Database.asset", typeof(ItemDatabaseObject));
        #else
        databaseObject = Resources.Load<ItemDatabaseObject>("Database");

    #endif
    }*/

        public void SwapItem(InventorySlot item1, InventorySlot item2)
        {
            if (item2.CanPlaceInSlot(item1.ItemObject) && item1.CanPlaceInSlot(item2.ItemObject))
            {
                InventorySlot temp = new InventorySlot(item2.item, item2.amount);
                item2.UpdateSlot(item1.item, item1.amount);
                item1.UpdateSlot(temp.item, temp.amount);
            }
        }

        public void RemoveItem(Item _item)
        {
            for (int i = 0; i < GetSlots.Length; i++)
            {
                if (GetSlots[i].item == _item)
                {
                    GetSlots[i].UpdateSlot(null,0);
                }
            }
        }
        [ContextMenu("Save")]
        public void Save()
        {
            // For JSON save
            /*string saveData = JsonUtility.ToJson(this, true);
        BinaryFormatter binaryFormatter = new BinaryFormatter();
        FileStream fileStream = File.Create(string.Concat(Application.persistentDataPath, savePath));
        binaryFormatter.Serialize(fileStream, saveData);
        fileStream.Close();*/

            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(string.Concat(Application.persistentDataPath, savePath), FileMode.Create, FileAccess.Write);
            formatter.Serialize(stream, container);
            stream.Close();

        }
        [ContextMenu("Load")]
        public void Load()
        {
            if (File.Exists(string.Concat(Application.persistentDataPath, savePath)))
            {
                // For JSON load
                /*BinaryFormatter binaryFormatter = new BinaryFormatter();
            FileStream fileStream = File.Open(string.Concat(Application.persistentDataPath, savePath), FileMode.Open);
            JsonUtility.FromJsonOverwrite(binaryFormatter.Deserialize(fileStream).ToString(),this);
            fileStream.Close();*/
            
                IFormatter formatter = new BinaryFormatter();
                Stream stream = new FileStream(string.Concat(Application.persistentDataPath, savePath), FileMode.Open, FileAccess.Read);
                Inventory newContainer = (Inventory)formatter.Deserialize(stream);
                for (int i = 0; i < GetSlots.Length; i++)
                {
                    GetSlots[i].UpdateSlot(newContainer.slots[i].item, newContainer.slots[i].amount);
                }
                stream.Close();
            }
        }

        [ContextMenu("Clear")]
        public void Clear()
        {
            container.Clear();
        }

      
    }

    [System.Serializable]
    public class Inventory
    {

        [SerializeField] internal InventorySlot[] slots = new InventorySlot[42];

        public void Clear()
        {
            for (int i = 0; i < slots.Length; i++)
            {
                slots[i].RemoveItem();
            }
        }
    }


    public delegate void SlotUpdated(InventorySlot _slot);

    [System.Serializable]
    public class InventorySlot
    {
        [System.NonSerialized] public UserInterface Parent;
        [System.NonSerialized] public GameObject SlotDisplay;
        [System.NonSerialized] public SlotUpdated OnAfterUpdate;
        [System.NonSerialized] public SlotUpdated OnBeforeUpdate;
        
        public ItemType[] AllowedItems = new ItemType[0];
        
        [SerializeField] internal Item item;
        [SerializeField] internal int amount;

        public ItemObject ItemObject
        {
            get
            {
                if (item.id >= 0)
                {
                    return Parent.inventoryObject.databaseObject.itemObjects[item.id];
                }

                return null;
            }
        }
    
        public InventorySlot()
        {
            UpdateSlot(new Item(), 0);
        }

        public InventorySlot(Item _item, int _amount)
        {
            UpdateSlot(_item, _amount);
        }

        public void UpdateSlot(Item _item, int _amount)
        {
            if (OnBeforeUpdate != null)
            {
                OnBeforeUpdate.Invoke(this);
            }
            item = _item;
            amount = _amount;
            if (OnAfterUpdate != null)
            {
                OnAfterUpdate.Invoke(this);
            }
        }

        public void RemoveItem()
        {
            UpdateSlot(new Item(), 0);
        }

        public void AddAmount(int value)
        {
            UpdateSlot(item, amount += value);
        }

        public bool CanPlaceInSlot(ItemObject _itemObject)
        {
            if (AllowedItems.Length <= 0 || _itemObject == null || _itemObject.data.id < 0)
                return true;
            for (int i = 0; i < AllowedItems.Length; i++)
            {
                if (_itemObject.type == AllowedItems[i])
                {
                    return true;
                }

                return false; 
            }
            return false;
        }
    }
}