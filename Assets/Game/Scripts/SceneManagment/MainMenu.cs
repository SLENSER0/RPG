using System;
using System.Collections;
using Game.Scripts.Saving;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Game.Scripts.SceneManagment
{
    public class MainMenu : MonoBehaviour
    {
        public GameObject LoadingScreen;
        public Slider LoadingSlider;

        [SerializeField] private Button newGameButton;
        [SerializeField] private Button loadGameButton;

        private void Start()
        {
            if (!DataPersistenceManager.instance.HasGameData())
            {
                loadGameButton.interactable = false;
            }
        }

        public void NewGame()
        {
            DataPersistenceManager.instance.NewGame();
            StartCoroutine(Transition());
        }
        
        public void LoadGame()
        {
            StartCoroutine(Transition());
        }

        public void QuitGame()
        {
            Application.Quit();
        }

        private IEnumerator Transition()
        {
            AsyncOperation operation = SceneManager.LoadSceneAsync("level0");
        
            LoadingScreen.SetActive(true);

            while (!operation.isDone)
            {
                float progressValue = Mathf.Clamp01(operation.progress / 0.9f);

                LoadingSlider.value = progressValue;

                yield return null;
            }
        }
    }
}
