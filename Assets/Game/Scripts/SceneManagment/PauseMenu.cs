using UnityEngine;

namespace Game.Scripts.SceneManagment
{
    public class PauseMenu : MonoBehaviour
    {

        public GameObject PauseMenuPanel;
        public bool IsPaused = false;
 
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (IsPaused)
                {
                    ResumeGame();
                }
                else
                {
                    PauseGame();
                }
            }
        }

        public void ResumeGame()
        {
            Time.timeScale = 1f;
            IsPaused = false;
            PauseMenuPanel.SetActive(false);
        }

        public void PauseGame()
        {
            Time.timeScale = 0f;
            IsPaused = true;
            PauseMenuPanel.SetActive(true);
        }


        public void QuitGame()
        {
            Application.Quit();
        }
    }
}
