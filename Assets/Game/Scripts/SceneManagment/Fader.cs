using System.Collections;
using UnityEngine;

namespace Game.Scripts.SceneManagment
{
   public class Fader : MonoBehaviour
   {
      private CanvasGroup _canvasGroup;

      private void Start()
      {
         _canvasGroup = GetComponent<CanvasGroup>();

         StartCoroutine(FadeOut(3f));
      }

      IEnumerator FadeOutIn()
      {
         yield return FadeOut(3f);
         print("Faded out");
         yield return FadeIn(1f);
         print("Faded in");
      }
 
      IEnumerator FadeOut(float time)
      {
         while (_canvasGroup.alpha < 1)
         {
            _canvasGroup.alpha += Time.deltaTime / time;
            yield return null;
         }
      }
   
      IEnumerator FadeIn(float time)
      {
         while (_canvasGroup.alpha > 0)
         {
            _canvasGroup.alpha -= Time.deltaTime / time;
            yield return null;
         }
      }
   }
}
